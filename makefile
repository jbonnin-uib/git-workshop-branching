all: build run

build:
	docker build -t py-snake .


run:
	docker run -it --rm --name running-py-snake py-snake
