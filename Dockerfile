FROM python:2.7-alpine

ADD src /

CMD [ "python2", "./run.py" ]
